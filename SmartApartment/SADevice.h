//
//  SADevice.h
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/5/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SADevice : NSObject {
    NSString *_deviceIdentifier;
    NSString *_deviceName;
    NSArray *_sensors;                                      //dictionary
    NSArray *_actuators;                                    //dictionary
}

-(id)initWithIdentifier:(NSString *)identifier;

@property (nonatomic) NSString *deviceIdentifier;
@property (nonatomic) NSString *deviceName;
@property (nonatomic) NSArray *actuators;//array of dictionaries with number and name
@property (nonatomic) NSArray *sensors;//array of dictionaries with number and name
@property (nonatomic) NSMutableDictionary *actuatorValues;
@property (nonatomic) NSMutableDictionary *sensorValues;
@end
