//
//  SADeviceConfigurationViewController.h
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/5/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SADevice.h"
#import "SAMQTTConnection.h"

@interface SADeviceConfigurationViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    SADevice *_device;
    SAMQTTConnection *_connection;
    SAStoredData *_sdata;
    IBOutlet UITableView *pinsTableView;
    IBOutlet UIButton *submitButton;
    IBOutlet UIButton *cancelButton;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(SADevice *)device storedData:(SAStoredData *)sdata connection:(SAMQTTConnection *)connection;
-(IBAction)submitConfiguration:(id)sender;
-(IBAction)cancelConfiguration:(id)sender;
@property (nonatomic, readonly) SADevice *device;
@property (nonatomic) SAMQTTConnection *connection;
@property (nonatomic) SAStoredData *sdata;
@end
