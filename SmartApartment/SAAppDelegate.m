//
//  SAAppDelegate.m
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/4/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "SAAppDelegate.h"

@implementation SAAppDelegate
@synthesize sdata=_sdata;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _sdata = [NSKeyedUnarchiver unarchiveObjectWithFile:[self pathForDataFile]];
    if (_sdata == nil) {
        _sdata = [[SAStoredData alloc] init];
    }
    connection = [[SAMQTTConnection alloc] init];
    [connection setSdata:_sdata];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] ==UIUserInterfaceIdiomPhone) {
        connectViewController = [[SAConnectViewController alloc] initWithNibName:@"SAConnectViewController_iPhone" bundle:nil sdata:_sdata];
    } else {
        connectViewController = [[SAConnectViewController alloc] initWithNibName:@"SAConnectViewController_iPad" bundle:nil sdata:_sdata];
    }
    [connectViewController setConnection:connection];
    
    navController = [[UINavigationController alloc] initWithRootViewController:connectViewController];
    navController.navigationBar.barStyle = UIBarStyleBlack;
    [self.window addSubview:navController.view];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (NSString *) pathForDataFile {
    NSArray *documentDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString*	path = nil;
 	
    if (documentDir) {
        path = [documentDir objectAtIndex:0];
    }
 	NSLog(@"path = %@/%@", path, @"StoredData.data");
    return [NSString stringWithFormat:@"%@/%@", path, @"StoredData.data"];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [NSKeyedArchiver archiveRootObject:_sdata toFile:[self pathForDataFile]];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
