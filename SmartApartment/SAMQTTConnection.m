//
//  SAMQTTConnection.m
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/4/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "SAMQTTConnection.h"
#include "TestSettings.h"

@implementation SAMQTTConnection
@synthesize sdata=_sdata, session=_session;

-(id)init{
    self = [super init];
    if (self) {
        connectionAttempts = 0;
    }
    return self;
}

-(void)connect {
    if (!self.sdata.connecting) {
        NSLog(@"starting session");

        self.session = [[MQTTSession alloc] initWithClientId:self.sdata.thisDeviceID];
        [self.session connectToHost:self.sdata.server port:[self.sdata.port intValue]];
        [self.session setDelegate:self];
        //[connectButton setTitle:@"Stop Connecting" forState:UIControlStateNormal];
        self.sdata.connecting = YES;
    } else {
        NSLog(@"closing session");
        NSMutableDictionary *messageDict = [[NSMutableDictionary alloc] init];
        [self.session close];
        self.session = nil;
        //       [connectButton setTitle:@"Connect" forState:UIControlStateNormal];
        self.sdata.connecting = NO;
    }
}

- (void)session:(MQTTSession*)sender handleEvent:(MQTTSessionEvent)eventCode {
    switch (eventCode) {
        case MQTTSessionEventConnected:
            NSLog(@"connected");
            connectionAttempts = 0;

            [self.session subscribeTopic:@"+/status"];
            [self.session subscribeTopic:@"+/configuration"];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"connected"
             object:self];
            break;
        case MQTTSessionEventConnectionRefused:
            NSLog(@"connection refused");
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"connection refused"
             object:self];
            break;
        case MQTTSessionEventConnectionClosed:
            NSLog(@"connection closed");
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"disconnected"
             object:self];
            break;
        case MQTTSessionEventConnectionError:
            NSLog(@"connection error");
            NSLog(@"reconnecting...");
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"reconnecting"
             object:self];
            // Forcing reconnection
            if (connectionAttempts < 5) {
                connectionAttempts++;
                [self.session connectToHost:self.sdata.server port:[self.sdata.port intValue]];
            } else {
                connectionAttempts = 0;
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"failed to connect"
                 object:self];
            }
            break;
        case MQTTSessionEventProtocolError:
            NSLog(@"protocol error");
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"protocol error"
             object:self];
            break;
    }
}

- (void)session:(MQTTSession*)sender
     newMessage:(NSData*)data
        onTopic:(NSString*)topic {
    NSLog(@"new message, %d bytes, topic=%@", [data length], topic);
    NSDictionary *receivedDataDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSString *packetTypeString = [receivedDataDictionary objectForKey:@"type"];
    NSString *originString = [receivedDataDictionary objectForKey:@"from"];
    if ([packetTypeString isEqualToString:@"configuration"]) {
        NSLog(@"Found a configuration packet");
        NSString *originString = [receivedDataDictionary objectForKey:@"from"];
        NSArray *pins = [receivedDataDictionary objectForKey:@"pins"];
        NSMutableArray *sensors = [[NSMutableArray alloc] init];
        NSMutableArray *actuators = [[NSMutableArray alloc] init];
        BOOL foundNewDevice = true;
        for (SADevice *device in self.sdata.devices) {
            if ([device.deviceIdentifier isEqualToString:originString]) {
                foundNewDevice = false;
                for (NSDictionary *pin in pins) {
                    NSString *pinState = [pin objectForKey:@"state"];
                    //NSString *pinName = [pin objectForKey:@"name"];
                    if ([pinState isEqualToString:@"input"]) {
                        [sensors addObject:pin];
                    } else if ([pinState isEqualToString:@"output"]) {
                        [actuators addObject:pin];
                    }
                }
                device.actuators = actuators;
                device.sensors = sensors;
            }
        }
        if (foundNewDevice == true) {
            for (NSDictionary *pin in pins) {
                NSString *pinState = [pin objectForKey:@"state"];
                if ([pinState isEqualToString:@"input"]) {
                    [sensors addObject:pin];
                } else if ([pinState isEqualToString:@"output"]) {
                    [actuators addObject:pin];
                }
            }
            NSLog(@"found new device");
            SADevice *newDevice = [[SADevice alloc] initWithIdentifier:originString];
            newDevice.sensors = sensors;
            newDevice.actuators = actuators;
            [self.sdata.devices addObject:newDevice];
            NSLog(@"devices count: %lu", (unsigned long)[self.sdata.devices count]);
        

            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DeviceFound"
                                                                object:self];
        }
        
        
      
        
        //this next section should be in a function
        NSArray *receivedSensorArray = [receivedDataDictionary objectForKey:@"sensors"];
        NSArray *receivedActuatorsArray = [receivedDataDictionary objectForKey:@"actuators"];
        for (SADevice *device in self.sdata.devices) {
            if ([device.deviceIdentifier isEqualToString:originString]) {
                NSLog(@"Received data for %@", device.deviceIdentifier);
                if ([self.sdata.displayedDeviceID isEqualToString:originString]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReceivedDataForDisplayedDevice"
                                                                        object:self];
                }
                for (NSDictionary *receivedSensor in receivedSensorArray) {
                    for (NSMutableDictionary *sensor in device.sensors) {
                        if ([[sensor objectForKey:@"number"] isEqualToString:[receivedSensor objectForKey:@"number"]]) {
                            NSLog(@"    Added sensor value: %@ for actuator: %@",[receivedSensor objectForKey:@"value"],[receivedSensor objectForKey:@"number"]);
                            [sensor setObject:[receivedSensor objectForKey:@"value"] forKey:@"value"];
                        }
                    }
                }
                for (NSDictionary *receivedActuator in receivedActuatorsArray) {
                    for (NSMutableDictionary *actuator in device.actuators) {
                        if ([[actuator objectForKey:@"number"] isEqualToString:[receivedActuator objectForKey:@"number"]]) {
                            NSLog(@"    Added actuator value: %@ for actuator: %@",[receivedActuator objectForKey:@"value"],[receivedActuator objectForKey:@"number"]);
                            [actuator setObject:[receivedActuator objectForKey:@"value"] forKey:@"value"];
                        }
                    }
                }
            }
        }
       
        
    //receiving values
    } else if ([packetTypeString isEqualToString:@"status"]) {
        NSArray *receivedSensorArray = [receivedDataDictionary objectForKey:@"sensors"];
        NSArray *receivedActuatorsArray = [receivedDataDictionary objectForKey:@"actuators"];
        for (SADevice *device in self.sdata.devices) {
            if ([device.deviceIdentifier isEqualToString:originString]) {
                NSLog(@"Received data for %@", device.deviceIdentifier);
                if ([self.sdata.displayedDeviceID isEqualToString:originString]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReceivedDataForDisplayedDevice"
                                                                        object:self];
                }
                for (NSDictionary *receivedSensor in receivedSensorArray) {
                    for (NSMutableDictionary *sensor in device.sensors) {
                        if ([[sensor objectForKey:@"number"] isEqualToString:[receivedSensor objectForKey:@"number"]]) {
                            if (VERBOSE) NSLog(@"    Added sensor value: %@ for actuator: %@",[receivedSensor objectForKey:@"value"],[receivedSensor objectForKey:@"number"]);
                            [sensor setObject:[receivedSensor objectForKey:@"value"] forKey:@"value"];
                        }
                    }
                }
                for (NSDictionary *receivedActuator in receivedActuatorsArray) {
                    for (NSMutableDictionary *actuator in device.actuators) {
                        if ([[actuator objectForKey:@"number"] isEqualToString:[receivedActuator objectForKey:@"number"]]) {
                            if (VERBOSE) NSLog(@"    Added actuator value: %@ for actuator: %@",[receivedActuator objectForKey:@"value"],[receivedActuator objectForKey:@"number"]);
                            [actuator setObject:[receivedActuator objectForKey:@"value"] forKey:@"value"];
                        }
                    }
                }
            }
        }
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"status updated"
         object:self];
    }

    

}

-(void)sendMessageString:(NSString *)string toNode:(NSString *)nodeID {
    NSLog(@"sending string message");
    NSData* pubData=[string dataUsingEncoding:NSUTF8StringEncoding];
    [self.session publishData:pubData onTopic:[NSString stringWithFormat:@"%@/command",nodeID]];
}

-(void)sendMessageData:(NSData *)data toNode:(NSString *)nodeID{
    NSLog(@"sending data message");
    [self.session publishData:data onTopic:[NSString stringWithFormat:@"%@/command",nodeID]];
}

-(void)hello:(NSString *)controller {
    NSLog(@"%@ sees connection", controller);
}


@end