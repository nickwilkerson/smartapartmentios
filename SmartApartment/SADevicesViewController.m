//
//  SADevicesViewController.m
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/5/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "SADevicesViewController.h"

@interface SADevicesViewController ()

@end

@implementation SADevicesViewController
@synthesize sdata=_sdata, connection=_connection;


- (id)initWithStyle:(UITableViewStyle)style storedData:(SAStoredData *)sdata connection:(SAMQTTConnection *)connection
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"Devices";
        _sdata = sdata;
        _connection = connection;
  /*      [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(deviceFound:)
                                                     name:@"DeviceFound"
                                                   object:nil];*/


    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"DeviceFound"
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note){
                                                      NSLog(@"device found reloading table view");
                                                      [self.tableView reloadData];
 
                                                  }];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSLog(@"numberOfRowsInSection: %lu", (unsigned long)[self.sdata.devices count]);
    return [self.sdata.devices count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"cellForRowAtIndexPath called for devices");
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.textLabel.text = [[self.sdata.devices objectAtIndex:indexPath.row] deviceIdentifier];
    
    return cell;
}
/*
-(void)deviceFound {
    NSLog(@"device found reloading table view");
    [self.tableView reloadData];
}*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SADeviceDetailViewController *detailDeviceView;
    SADevice *device = [self.sdata.devices objectAtIndex:indexPath.row];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        detailDeviceView = [[SADeviceDetailViewController alloc] initWithNibName:@"SADeviceDetailViewController_iPhone" bundle:Nil device:device storedData:self.sdata connection:self.connection];
    } else {
        detailDeviceView = [[SADeviceDetailViewController alloc] initWithNibName:@"SADeviceDetailViewController_iPad" bundle:Nil device:device storedData:self.sdata connection:self.connection];
    }
    [self.navigationController pushViewController:detailDeviceView animated:YES];
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
