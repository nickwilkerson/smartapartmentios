//
//  SAStoredData.m
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/4/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "SAStoredData.h"

@implementation SAStoredData
@synthesize server=_server, port=_port, devices=_devices, thisDeviceID=_thisDeviceID, displayedDeviceID=_displayedDeviceID;

-(id)init{
    NSLog(@"init called for SAStoredData");
    self = [super init];
    if (self) {
        self.connecting = NO;
        _devices = [[NSMutableArray alloc] init];
        _server = @"mosquittopi.local";
        _port = @"1883";
        _displayedDeviceID = @"";
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)coder {
    NSLog(@"initWithCoder");
    self = [super init];
    if (self) {
        _devices = [[coder decodeObjectForKey:@"devices"] mutableCopy];
        _server = [coder decodeObjectForKey:@"server"];
        _port = [coder decodeObjectForKey:@"port"];
        _displayedDeviceID = @"";
        self.connecting = NO;

    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)coder {
    NSLog(@"encodeWithCoder");
    [coder encodeObject:_devices forKey:@"devices"];
    [coder encodeObject:_server forKey:@"server"];
    [coder encodeObject:_port forKey:@"port"];
}



@end
