//
//  SADeviceDetailViewController.h
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/5/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SADevice.h"
#import "SAMQTTConnection.h"
#import "SADeviceConfigurationViewController.h"

@interface SADeviceDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UITableView *sensorsTableView;
    IBOutlet UITableView *actuatorsTableView;
    IBOutlet UIButton *configureButton;
    SADevice *_device;
    SAMQTTConnection *_connection;
    SAStoredData *_sdata;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(SADevice *)device storedData:(SAStoredData *)sdata connection:(SAMQTTConnection *)connection;
-(IBAction)configureDevice:(id)sender;
@property (nonatomic, readonly) SADevice *device;
@property (nonatomic) SAMQTTConnection *connection;
@property (nonatomic) SAStoredData *sdata;
@end
