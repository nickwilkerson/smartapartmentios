//
//  SAConnectViewController.m
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/4/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//


#import "SAConnectViewController.h"

@interface SAConnectViewController ()

@end

@implementation SAConnectViewController

@synthesize sdata=_sdata, connection=_connection;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil sdata:(SAStoredData *)sdata;
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _sdata = sdata;
        self.title = @"Connection";
        activityIndicator.hidesWhenStopped = YES;
        [activityIndicator setHidden:YES];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(connected:)
                                                     name:@"connected"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(disconnected:)
                                                     name:@"disconnected"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(connectFailed:)
                                                     name:@"failed to connect"
                                                   object:nil];


    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    serverField.text = self.sdata.server;
    portField.text = self.sdata.port;
    deviceIDField.text = self.sdata.deviceID;
    [connectButton setBackgroundColor:[UIColor blueColor]];
    [connectButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [showDevicesButton setHidden:YES];
    [showDevicesButton setBackgroundColor:[UIColor blueColor]];
    [showDevicesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)connect:(id)sender{
    NSLog(@"connect pressed");
    self.sdata.server = [serverField text];
    NSLog(@"server: %@", [serverField text]/*self.sdata.server*/);
    self.sdata.port = [[portField.text componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet]invertedSet]] componentsJoinedByString:@""];
    self.sdata.thisDeviceID = [deviceIDField text];
    NSLog(@"port: %@", self.sdata.port);
    [activityIndicator setHidden:NO];
    [activityIndicator startAnimating];
    [self.connection connect];
}





-(void)connected:(NSNotification *)notification {
    [connectButton setTitle:@"Disconnect" forState:UIControlStateNormal];
    serverField.textColor = [UIColor grayColor];
    portField.textColor = [UIColor grayColor];
    deviceIDField.textColor = [UIColor grayColor];
    serverField.enabled = NO;
    portField.enabled = NO;
    deviceIDField.enabled = NO;

    [activityIndicator stopAnimating];
    [activityIndicator setHidden:YES];
    [showDevicesButton setHidden:NO];
  //  NSMutableDictionary *messageDict = [[NSMutableDictionary alloc] init];
    SADevicesViewController *devicesViewController;
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    devicesViewController = [[SADevicesViewController alloc] initWithStyle:UITableViewStylePlain storedData:self.sdata connection:self.connection];
    if (self.isViewLoaded && self.view.window) {//if connect view controller is visible
        [self.navigationController pushViewController:devicesViewController animated:YES];
    }
    //[messageDict setObject:@"player added" forKey:@"event"];

  //    [self.connection sendMessageData:[NSJSONSerialization dataWithJSONObject:messageDict options:NSJSONWritingPrettyPrinted error:nil]];


    
    
}

-(IBAction)showDevices:(id)sender {
    SADevicesViewController *devicesViewController;
    devicesViewController = [[SADevicesViewController alloc] initWithStyle:UITableViewStylePlain storedData:self.sdata connection:self.connection];
    [self.navigationController pushViewController:devicesViewController animated:YES];

}

-(void)disconnected:(NSNotification *)notification {
    [connectButton setTitle:@"Connect" forState:UIControlStateNormal];
    serverField.textColor = [UIColor blackColor];
    portField.textColor = [UIColor blackColor];
    deviceIDField.textColor = [UIColor blackColor];
    serverField.enabled = YES;
    portField.enabled = YES;
    deviceIDField.enabled = YES;
    [activityIndicator stopAnimating];
    [activityIndicator setHidden:YES];
    [showDevicesButton setHidden:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)connectFailed:(NSNotification *)notification {
    NSLog(@"Connection Failed");
    [activityIndicator stopAnimating];
    UIAlertView *connectFailedAlert = [[UIAlertView alloc] initWithTitle:@"Connection Error"
                                                                 message:@"Failed to Connect"
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
    [connectFailedAlert show];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [connectButton setTitle:@"Connect" forState:UIControlStateNormal];
}




-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
