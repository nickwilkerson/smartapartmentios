//
//  SADeviceConfigurationViewController.m
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/5/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "SADeviceConfigurationViewController.h"

@interface SADeviceConfigurationViewController ()

@end

@implementation SADeviceConfigurationViewController
@synthesize device=_device, sdata=_sdata, connection=_connection;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(SADevice *)device storedData:(SAStoredData *)sdata connection:(SAMQTTConnection *)connection
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _device=device;
        _sdata=sdata;
        _connection=connection;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    submitButton.backgroundColor = [UIColor blueColor];
    submitButton.titleLabel.textColor = [UIColor whiteColor];
    cancelButton.backgroundColor = [UIColor blueColor];
    cancelButton.titleLabel.textColor = [UIColor whiteColor];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"ReceivedDataForDisplayedDevice"
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note){
                                                      NSLog(@"DetailViewController received data - reloading table view");
                                                      [pinsTableView reloadData];
                                                  }];
    // Do any additional setup after loading the view from its nib.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.device.sensors count] + [self.device.actuators count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    if (indexPath.row < [self.device.sensors count]) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@ - input",[[self.device.sensors objectAtIndex:indexPath.row] objectForKey:@"number"]];
    } else {
        cell.textLabel.text = [NSString stringWithFormat:@"%@ - output",[[self.device.actuators objectAtIndex:(indexPath.row - [self.device.sensors count])] objectForKey:@"number"]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}



-(IBAction)submitConfiguration:(id)sender {

}

-(IBAction)cancelConfiguration:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
