//
//  SADevicesViewController.h
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/5/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAStoredData.h"
#import "SAMQTTConnection.h"
#import "SADeviceDetailViewController.h"

@interface SADevicesViewController : UITableViewController <UITableViewDelegate> {
    SAStoredData *_sdata;
    SAMQTTConnection *_connection;
}

- (id)initWithStyle:(UITableViewStyle)style storedData:(SAStoredData *)sdata connection:(SAMQTTConnection *)connection;

@property (nonatomic) SAStoredData *sdata;
@property (nonatomic) SAMQTTConnection *connection;
@end
