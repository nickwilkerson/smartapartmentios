//
//  SAMQTTConnection.h
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/4/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import "SAStoredData.h"
#import "MQTTSession.h"


@interface SAMQTTConnection : NSObject {
    SAStoredData *_sdata;
    MQTTSession *_session;
    
    int connectionAttempts;
}

#pragma mark - MQTT Callback methods
- (void)session:(MQTTSession*)sender handleEvent:(MQTTSessionEvent)eventCode;
- (void)session:(MQTTSession*)sender newMessage:(NSData*)data onTopic:(NSString*)topic;

-(void)connect;

-(void)sendMessageString:(NSString *)string toNode:(NSString *)nodeID;
-(void)sendMessageData:(NSData *)data toNode:(NSString *)nodeID;

-(void)hello:(NSString *)controller;

@property (nonatomic, strong) SAStoredData *sdata;
@property (nonatomic, strong) MQTTSession *session;
@end
