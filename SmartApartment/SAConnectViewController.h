//
//  SAConnectViewController.h
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/4/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAStoredData.h"
#import "SAMQTTConnection.h"
#import "SADevicesViewController.h"

@interface SAConnectViewController : UIViewController <UITextFieldDelegate> {
    IBOutlet UITextField *serverField;
    IBOutlet UITextField *portField;
    IBOutlet UITextField *deviceIDField;
    IBOutlet UIButton *connectButton;
    IBOutlet UIButton *showDevicesButton;

    
    IBOutlet UIActivityIndicatorView *activityIndicator;
    
    SAMQTTConnection *_connection;
    SAStoredData *_sdata;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil sdata:(SAStoredData *)sdata;

-(IBAction)connect:(id)sender;
-(IBAction)showDevices:(id)sender;

@property (nonatomic) SAMQTTConnection *connection;
@property (nonatomic) SAStoredData *sdata;

@end
