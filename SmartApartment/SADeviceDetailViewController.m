//
//  SADeviceDetailViewController.m
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/5/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "SADeviceDetailViewController.h"
#include "TestSettings.h"
@interface SADeviceDetailViewController ()

@end

@implementation SADeviceDetailViewController
@synthesize device=_device, sdata=_sdata, connection=_connection;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(SADevice *)device storedData:(SAStoredData *)sdata connection:(SAMQTTConnection *)connection
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _sdata = sdata;
        _device=device;
        _connection = connection;
        self.title = device.deviceIdentifier;
        actuatorsTableView = [[UITableView alloc] init];
        sensorsTableView = [[UITableView alloc] init];
        [self.connection hello:@"DeviceDetailViewController"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    configureButton.titleLabel.textColor = [UIColor whiteColor];
    configureButton.backgroundColor = [UIColor blueColor];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"ReceivedDataForDisplayedDevice"
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note){
                                                      NSLog(@"DetailViewController received data - reloading table view");
                                                      [sensorsTableView reloadData];
                                                      [actuatorsTableView reloadData];
                                                  }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfCells;
    if ([tableView isEqual:actuatorsTableView]) {
        NSLog(@"requested number of actuators");
        numberOfCells = [self.device.actuators count];
    } else if ([tableView isEqual:sensorsTableView]) {
        NSLog(@"requested number of sensors");
        numberOfCells = [self.device.sensors count];
    } else {
        numberOfCells = 0;
    }
    NSLog(@"number of cells: %d", (long)numberOfCells);
    return numberOfCells;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (VERBOSE) NSLog(@"refreshing cell");
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(tableView == sensorsTableView) {
        NSMutableDictionary *sensor = [self.device.sensors objectAtIndex:indexPath.row];
        NSString *sensorNumber = [sensor objectForKey:@"number"];
        NSString *sensorName = [sensor objectForKey:@"name"];
        if (sensorName == nil) {
            sensorName = @"";
        }
        NSString *sensorValue = [sensor objectForKey:@"value"];
        cell.textLabel.text = [NSString stringWithFormat:@"GPIO %@ %@: %@",sensorNumber,sensorName,sensorValue];
    } else if (tableView == actuatorsTableView) {
        NSMutableDictionary *actuator = [self.device.actuators objectAtIndex:indexPath.row];
        NSString *actuatorNumber = [actuator objectForKey:@"number"];
        NSString *actuatorName = [actuator objectForKey:@"name"];
        if (actuatorName == nil) {
            actuatorName = @"";
        }
        NSString *actuatorValue = [actuator objectForKey:@"value"];
        cell.textLabel.text = [NSString stringWithFormat:@"GPIO %@ %@: %@",actuatorNumber,actuatorName,actuatorValue];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *toString = self.device.deviceIdentifier;
    if(tableView == sensorsTableView) {
        NSLog(@"selected sensor tableView");
    }else if (tableView == actuatorsTableView) {
        NSMutableDictionary *sendDataDictionary = [[NSMutableDictionary alloc] init];
        NSMutableArray *actuatorsToSend = [[NSMutableArray alloc] init];
        NSMutableDictionary *actuatorToSend = [[NSMutableDictionary alloc] init];
        NSLog(@"selected actuator tableView");
        
        NSDictionary *storedActuator = [self.device.actuators objectAtIndex:indexPath.row];
        NSString *valueToSend;
        NSLog(@"value: %@", [[self.device.actuators objectAtIndex:indexPath.row] objectForKey:@"value"]);
        if ([[storedActuator objectForKey:@"value"] isEqualToString:@"1"]) {
            if (VERBOSE) NSLog(@"current value is 1 setting to 0");
            valueToSend = @"0";
        } else if ([[storedActuator objectForKey:@"value"] isEqualToString:@"0"]) {
            if (VERBOSE) NSLog(@"current value is 0 setting to 1");
            valueToSend = @"1";
        } else {
            if (VERBOSE) NSLog(@"unrecognized actuator value found when trying to send");
            valueToSend = @"";
        }
        [actuatorToSend setObject:[storedActuator objectForKey:@"number"] forKey:@"number"];
        [actuatorToSend setObject:valueToSend forKey:@"value"];
        [actuatorsToSend addObject:actuatorToSend];
        [sendDataDictionary setObject:@"command" forKey:@"type"];
        [sendDataDictionary setObject:toString forKey:@"to"];
        [sendDataDictionary setObject:actuatorsToSend forKey:@"actuators"];
        NSData *jsonDataToSend = [NSJSONSerialization dataWithJSONObject:sendDataDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString* outputString = [NSString stringWithUTF8String:[jsonDataToSend bytes]];
        NSLog(@"sending: %@", outputString);
        [self.connection sendMessageData:jsonDataToSend toNode:toString];
        NSLog(@"actuator: %@ sent value: %@", [storedActuator objectForKey:@"number"],valueToSend);
    }
}

-(void)viewWillAppear:(BOOL)animated {
    self.sdata.displayedDeviceID = self.device.deviceIdentifier;
}

-(void)viewWillDisappear:(BOOL)animated {
    self.sdata.displayedDeviceID = @"";
}



-(IBAction)configureDevice:(id)sender {
    SADeviceConfigurationViewController *configurationViewController;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        configurationViewController = [[SADeviceConfigurationViewController alloc] initWithNibName:@"SADeviceConfigurationViewController_iPhone" bundle:nil device:self.device storedData:self.sdata connection:self.connection];
    } else {
        configurationViewController = [[SADeviceConfigurationViewController alloc] initWithNibName:@"SADeviceConfigurationViewController_iPad" bundle:nil device:self.device storedData:self.sdata connection:self.connection];
    }
    [self.navigationController pushViewController:configurationViewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
