//
//  SADevice.m
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/5/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import "SADevice.h"

@implementation SADevice
@synthesize sensors=_sensors,actuators=_actuators,deviceIdentifier=_deviceIdentifier, deviceName=_deviceName,sensorValues=_sensorValues, actuatorValues=_actuatorValues;

-(id)initWithIdentifier:(NSString *)identifier {
    self = [super init];
    if (self) {
        _deviceIdentifier = identifier;
    }
    return self;
}

@end
