//
//  SAStoredData.h
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/4/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SADevice.h"

@interface SAStoredData : NSObject <NSCoding>{
    NSMutableArray *_devices;
    NSString *_server;
    NSString *_port;
    NSString *_thisDeviceID;
    NSString *_displayedDeviceID;
}

@property (nonatomic, strong) NSString *server;
@property (nonatomic, strong) NSString *port;
@property (nonatomic) NSString *deviceID;
@property (nonatomic) NSMutableArray *devices;
@property (nonatomic) BOOL connecting;
@property (nonatomic) NSString *thisDeviceID;
@property (nonatomic) NSString *displayedDeviceID;
@end
