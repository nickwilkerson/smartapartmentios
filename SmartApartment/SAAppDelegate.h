//
//  SAAppDelegate.h
//  SmartApartment
//
//  Created by Nicholas Wilkerson on 12/4/13.
//  Copyright (c) 2013 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAConnectViewController.h"
#import "SAMQTTConnection.h"
#import "SAStoredData.h"

@interface SAAppDelegate : UIResponder <UIApplicationDelegate> {
    UINavigationController *navController;
    SAStoredData *_sdata;
    SAMQTTConnection *connection;
    SAConnectViewController *connectViewController;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SAStoredData *sdata;

@end
